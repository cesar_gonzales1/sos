import './App.css';
import React, { Component } from 'react';
import { render } from "react-dom";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'jquery/dist/jquery.min.js';
import Login from "./usuario/pages/login/login-controller";
import 'popper.js/dist/umd/popper.min.js';
import Datos from './usuario/pages/datos/datos-controller';
import Inicio from './usuario/pages/inicio/inicio-controller';
import Detencion from './usuario/pages/detenido/detenido-controller';
import DetenidoRespuesta from './usuario/pages/detenido-respuesta/detenido-respuesta-controller';
import CallBack from './usuario/pages/twitter-redirect/callback-controller';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            {/* <Route
              path="/"
              component={Login} /> */}
            <Route
              exact
              path="/"
              component={Login} />
            <Route
              path="/inicio"
              component={Inicio} />
            <Route
              path="/detencion/:tipo"
              component={Detencion} />
            <Route
              path="/datos"
              component={Datos} />
            <Route
              path="/detencion-respuesta"
              component={DetenidoRespuesta} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
