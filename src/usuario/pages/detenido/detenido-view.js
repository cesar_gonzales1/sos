import React, { Fragment } from 'react';
import './style.css';
import '../../../public/css/index.css';
import Select from 'react-select';
import BackButton from '../back/back-controller';
import LogoutIcon from '../../../public/images/logout.svg';

function View(props) {

    const {
        tipoemergencia,
        idpersona,
        ubigeo,
        documento,
        nombres,
        correo,
        celular,
        detalleincidente,
        detencionRespuestaHandler,
        handleChangeForm,
        handleSelectChange,
        lista,
        regresarHandler,
        defaultVal,
        logout
    } = props;

    return (
        <Fragment>
            <div class="container">
                <form>
                    <div className="row justify-content-md-center mt-3">

                        <div className="col-10 col-md-10 col-sm-10 col-lg-10">
                            <BackButton />
                        </div>

                        <div className="col-2 col-md-2 col-sm-2 col-lg-2">
                            <a onClick={logout} href="#">
                                <img src={LogoutIcon} alt="Logout" width="30" />
                            </a>
                        </div>

                        <div className="col-md-12 col-lg-6 mb-3">
                            <h3 className="text-center">Confirma tus datos para contactar a un abogado de nuestra red de voluntarios:</h3>

                            <input
                                placeholder="DNI / CE / PASAPORTE"
                                type="text"
                                maxLength="8"
                                name="documento"
                                onChange={handleChangeForm}
                                value={documento}
                                className="form-control mb-3 mt-3"
                            />

                            <input
                                placeholder="NOMBRES Y APELLIDOS"
                                type="text"
                                name="nombres"
                                value={nombres}
                                onChange={handleChangeForm}
                                className="form-control mb-3"
                            />

                            <input
                                placeholder="CORREO ELECTRÓNICO"
                                type="text"
                                name="correo"
                                value={correo}
                                onChange={handleChangeForm}
                                className="form-control mb-3"
                            />

                            <input
                                placeholder="CELULAR"
                                type="text"
                                name="celular"
                                maxLength="9"
                                value={celular}
                                onChange={handleChangeForm}
                                className="form-control mb-3"
                            />

                            <Select
                                defaultValue={{ label: 'LIMA - LIMA - LIMA', value: 1251 }}
                                options={lista}
                                isDisabled={false}
                                isLoading={false}
                                isRtl={false}
                                isSearchable={true}
                                onChange={handleSelectChange}
                                placeholder="Seleccione un ubigeo"
                            />

                            <textarea
                                placeholder="DETALLE DEL INCIDENTE / OBSERVACIONES"
                                name="detalleincidente"
                                rows="4"
                                cols="4"
                                onChange={handleChangeForm}
                                value={detalleincidente}
                                className="form-control mb-3 mt-3"
                            />

                            <button onClick={detencionRespuestaHandler} className="btn btn-main col-md-12 mb-3">ENVIAR</button>
                            {/* <button onClick={regresarHandler} className="btn btn-main col-md-12 mb-3">REGRESAR</button> */}
                        </div>
                    </div>
                </form>
            </div>
        </Fragment >
    );
}
export default View;
