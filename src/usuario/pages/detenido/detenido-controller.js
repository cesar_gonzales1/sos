
import React from 'react';
import View from './detenido-view';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class Detenido extends React.Component {

    state = {
        tipoemergencia: '',
        idpersona: '',
        ubigeo: '',
        documento: '',
        nombres: '',
        correo: '',
        celular: '',
        detalleincidente: '',
        lista: []
    }

    async componentDidMount() {
        var id = localStorage.getItem('userId');
        if (id == null) {
            this.props.history.push('/');
            // window.location.replace('/');
        }

        const { tipo } = this.props.match.params;

        axios.get(`${URL}/api/v1/Persona/obtener/${id}`)
            .then(res => {
                this.setState({
                    documento: res.data.data.documento,
                    nombres: res.data.data.nombre,
                    ubigeo: 1251,
                    celular: res.data.data.celular,
                    correo: res.data.data.correo,
                    tipoemergencia: tipo
                });
            }).catch(err => {
                alert("Error al registrarse.");
            });

        axios.get(`${URL}/api/v1/Ubigeo/listado`)
            .then(res => {
                let ubigeoList = res.data.data;
                if (res) {
                    ubigeoList = ubigeoList.map(x => ({ label: x.nombre, value: x.codigo }));
                }
                this.setState({
                    lista: ubigeoList
                });
            }).catch(err => {
                alert("Error al registrarse.");
            });
    }

    render() {
        return (
            <View
                tipoemergencia={this.state.tipoemergencia}
                idpersona={this.state.idpersona}
                ubigeo={this.state.ubigeo}
                documento={this.state.documento}
                nombres={this.state.nombres}
                correo={this.state.correo}
                celular={this.state.celular}
                detalleincidente={this.state.detalleincidente}
                detencionRespuestaHandler={this.state.tipoemergencia}
                handleChangeForm={this.handleChangeForm}
                detencionRespuestaHandler={this.detencionRespuestaHandler}
                handleSelectChange={this.handleSelectChange}
                lista={this.state.lista}
                regresarHandler={this.regresarHandler}
                default={this.state.default}
                logout={this.logout}
            />
        );
    }

    handleSelectChange = async (option, action) => {
        // option.value
        this.setState({
            ubigeo: option.value,
        });
    }

    regresarHandler = async (e) => {
        this.props.history.push('/inicio');
        // window.location.replace('/inicio');
    }

    handleChangeForm = (e) => {
        const value = e.target.value;
        this.setState({
            [e.target.name]: value
        });
    }

    logout = (e) => {
        e.preventDefault();
        localStorage.clear();
        window.location.replace('/');
    }

    detencionRespuestaHandler = async (e) => {
        e.preventDefault();

        var id = localStorage.getItem('userId');

        const user = {
            "tipoemergencia": parseInt(this.state.tipoemergencia),
            "idpersona": parseInt(id),
            "ubigeo": parseInt(this.state.ubigeo),
            "documento": this.state.documento,
            "nombres": this.state.nombres,
            "correo": this.state.correo,
            "celular": this.state.celular,
            "detalleincidente": this.state.detalleincidente
        };

        axios.post(`${URL}/api/v1/Incidente/registrarIncidente`, user)
            .then(res => {
                this.props.history.push('/detencion-respuesta');
                // window.location.replace('/detencion-respuesta');
            }).catch(err => {
                alert("Error al modificar sus datos.");
            });
    }

}

export default withRouter(
    (Detenido)
)