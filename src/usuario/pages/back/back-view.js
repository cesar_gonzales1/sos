import React from 'react';
import { Link } from 'react-router-dom'
import BackIcon from '../../../public/images/left-chevron.svg';

function View(props) {
    const {
        goBack
    } = props;
    return (
        <Link to="#" onClick={goBack}>
            <img src={BackIcon} className="rounded-circle" alt="Cinque Terre" width="30" />
        </Link>
    );
}
export default View;