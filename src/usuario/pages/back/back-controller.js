import React from 'react';
import { withRouter } from 'react-router-dom'
import View from './back-view';

class BackButton extends React.Component {
    goBack = (e) => {
        e.preventDefault();
        this.props.history.goBack();
    }
    render() {
        return (
            <View
                goBack={this.goBack}
            />
        );
    }
}
export default withRouter(
    (BackButton)
);