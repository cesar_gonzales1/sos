import React from 'react';
import View from './datos-view';
import axios from 'axios';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';

class Datos extends React.Component {

    state = {
        documento: '',
        nombres: '',
        celular_contacto: '',
        celular: '',
        correo: '',
    }

    async componentDidMount() {

        var id = localStorage.getItem('userId');

        if (id == null) {
            // window.location.replace('/');
            this.props.history.push('/');
        }

        axios.get(`${URL}/api/v1/Persona/obtener/${id}`)
            .then(res => {
                this.setState({
                    documento: res.data.data.documento,
                    nombres: res.data.data.nombre,
                    celular_contacto: res.data.data.celular_contacto,
                    celular: res.data.data.celular,
                    correo: res.data.data.correo
                });
            }).catch(err => {
                alert("Error al cargar.");
            });
    }

    handleChangeForm = (e) => {
        const value = e.target.value;
        this.setState({
            [e.target.name]: value
        });
    }

    render() {
        return (
            <View
                datosHandler={this.datosHandler}
                handleChangeForm={this.handleChangeForm}
                documento={this.state.documento}
                nombres={this.state.nombres}
                celular_contacto={this.state.celular_contacto}
                celular={this.state.celular}
                correo={this.state.correo}
                logout={this.logout}
            />
        );
    }

    logout = (e) => {
        e.preventDefault();
        localStorage.clear();
        window.location.replace('/');
    }

    datosHandler = async (e) => {
        e.preventDefault();
        var id = localStorage.getItem('userId');
        const user = {
            "idpersona": parseInt(id),
            "documento": this.state.documento,
            "nombre": this.state.nombres,
            "correo": this.state.correo,
            "celular": this.state.celular,
            "celular_contacto": this.state.celular_contacto
        }

        axios.put(`${URL}/api/v1/Persona/actualizar`, user)
            .then(res => {
                localStorage.setItem('nombre', this.state.nombres);
                Swal.fire({
                    title: 'Mensaje',
                    text: "Sus datos fueron actualizados correctamente.",
                    type: 'info',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                }).then(async (result) => {
                    if (result.value) {
                        this.props.history.push('/inicio');
                        // window.location.replace('/inicio');
                    }
                });
            }).catch(err => {
                alert("Error al modificar sus datos.");
            });
    }

}
export default withRouter(
    (Datos)
)
