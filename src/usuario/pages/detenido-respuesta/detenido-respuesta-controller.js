import React from 'react';
import View from './detenido-respuesta-view';
import { withRouter } from 'react-router-dom';

class DetenidoRespuesta extends React.Component {

    async componentDidMount() {

        var id = localStorage.getItem('userId');

        if (id == null) {
            // this.props.history.push('/');
            window.location.replace('/');
        }
    }

    logout = (e) => {
        e.preventDefault();
        localStorage.clear();
        window.location.replace('/');
        }

    goTo = () => {
        window.open("https://www.instagram.com/p/CHlEvamhvgi/?igshid=1qtn0grda9v67", '_blank');
    }

    goto2 = () => {
        window.open("https://a2j.documate.org/interview?i=docassemble.playground45%3AHabeas+Corpus+detencin+protesta.yml#page2", '_blank');
    }

    render() {
        return (
            <View
                logout={this.logout}
                goTo={this.goTo}
                goto2={this.goto2}
            />
        );
    }
}
export default withRouter(
    (DetenidoRespuesta)
)