import React, { Fragment } from 'react';
import './style.css';
import '../../../public/css/index.css';
import BackButton from '../back/back-controller';
import LogoutIcon from '../../../public/images/logout.svg';

function View(props) {

    const {
        logout,
        goTo,
        goto2
    } = props;

    return (
        <Fragment>
            <div className="container">
                <form>
                    <div className="row justify-content-md-center mt-3">
                        <div className="col-10 col-md-10 col-sm-10 col-lg-10">
                            <BackButton />
                        </div>
                        <div className="col-2 col-md-2 col-sm-2 col-lg-2">
                            <a onClick={logout} href="#">
                                <img src={LogoutIcon} alt="Logout" width="30" />
                            </a>
                        </div>
                        <div className="col-md-12 col-lg-6 mb-3">
                            <h3 className="text-center">Tranquil@, estamos gestionando la ayuda. ¿Cómo te vamos a apoyar?</h3>
                            <p className="col-md-12 mb-3"> 1. Estamos verificando tu solicitud y se la enviaremos a uno de los abogados voluntarios.</p>
                            <p className="col-md-12 mb-3"> 2. En aproximadamente 60 minutos recibirás la información de los abogados que pueden ayudarte en tu caso para que los puedas contactar.</p>
                            <p className="col-md-12 mb-3"> 3. Al mismo tiempo le enviaremos tus datos personales y la información que detallaste a estos abogados para que sepan que te pondrás en contacto con ellos.</p>
                            <p className="col-md-12 mb-3"> Mientras tanto, te brindamos la siguiente información de interés:</p>
                            <button onClick={goTo} className="btn btn-main col-md-12 mb-3">¿Qué hago si me detienen?</button>
                            <button onClick={goto2} className="btn btn-main col-md-12 mb-3">Generar un Habeas Corpus</button>
                        </div>
                    </div>
                </form>
            </div>
        </Fragment >
    );
}
export default View;
