import React, { Fragment } from 'react';
import './style.css';
import TwitterLogin from "react-twitter-login";
import FacebookLogin from 'react-facebook-login';
import logo from "../../../public/images/logo.png";
import axios from 'axios';
import URL from '../../helpers/index';

function View(props) {

    const {

    } = props;

    const responseTwitter = (response) => {

        console.log(response);

        localStorage.setItem('userSocial', response.id);
        localStorage.setItem('nombre', response.name);

        const user = {
            "tipored": 2,
            "socialID": response.id,
            "documento": "",
            "nombres": response.name,
            "correo": response.email,
            "celular": "",
            "celular_contacto": ""
        };

        console.log(response);

        // axios.post(`${URL}/api/v1/Persona/registro`, user)
        //     .then(res => {
        //         localStorage.setItem('userId', res.data.data.id);
        //         window.location.replace('/inicio');
        //     }).catch(() => {
        //         alert("Error al registrarse.");
        //     });
    }

    const responseFacebook = async (response) => {

        localStorage.setItem('userSocial', response.id);
        localStorage.setItem('nombre', response.name);

        if (response.id == undefined) {
            alert("No pudo iniciar sesión en facebook.");
            return;
        }

        const user = {
            "tipored": 1,
            "socialID": response.id,
            "documento": "",
            "nombres": response.name,
            "correo": response.email,
            "celular": "",
            "celular_contacto": ""
        };

        const loginUser = {
            "tipored": 1,
            "socialID": response.id
        }

        var loginResponse = await axios.post(`${URL}/api/v1/Persona/login`, loginUser);

        var userId = await loginResponse.data.data.id;

        localStorage.setItem('userId', userId);

        if (userId == 0) {
            var registerResponse = await axios.post(`${URL}/api/v1/Persona/registro`, user);

            var status = await registerResponse.data.status;

            if (status == 401) {
                localStorage.removeItem('userSocial');
                localStorage.removeItem('nombre');
                alert("Su usuario fue baneado!");
                return;
            }

            var newId = await registerResponse.data.data.id;

            localStorage.setItem('userId', newId);
            window.location.replace('/inicio');
            // this.props.history.push('/inicio');
            return;
        }
        // this.props.history.push('/inicio');
        window.location.replace('/inicio');
    }

    return (
        <Fragment>

            <div className="container h-100">
                <div className="row justify-content-center align-items-center h-100">
                    <div className="col-md-12 col-lg-6 mb-3 mt-3">

                        <img src={logo} width="200" className="img-thumbnail mx-auto d-block " />

                        {/* <div className="row justify-content-center mt-3">
                            <TwitterLogin
                                authCallback={responseTwitter}
                                consumerKey={"7F7U8GagFtMSahxLuA7xebGLP"}
                                consumerSecret={"xjiHV9aAXyXu0SzdPM5hOVbhOLegHhpRFqqtCJyGnlOObWRkW4"}
                            />
                        </div> */}

                        <div className="row justify-content-center mt-3">
                            <FacebookLogin
                                appId="1655464401301035"
                                textButton="Ingresa con Facebook"
                                fields="name,email,picture"
                                callback={responseFacebook}
                            />
                        </div>

                        <div className="row justify-content-center mt-3">
                            <label><input checked="checked" readOnly type="checkbox" />Acepto términos y condiciones</label>

                        </div>

                    </div >
                </div>
            </div >
            <div>
            </div>
        </Fragment >
    );
}
export default View;
