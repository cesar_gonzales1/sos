import { withRouter } from 'react-router-dom';
import React from 'react';
import View from './login-view';

class Login extends React.Component {

    state = {}

    async componentDidMount() {
        var item = localStorage.getItem('userSocial');
        if (item) {
            this.props.history.push('/inicio');
            // window.location.replace('/inicio');
        }
    }

    render() {
        return (
            <View />
        );
    }

}

export default withRouter(
    (Login)
)
