import React, { Fragment } from 'react';
import './style.css';
import gavel from '../../../public/images/gavel-white-18dp.svg';
import hospital from '../../../public/images/local_hospital-24px.svg';
import LogoutIcon from '../../../public/images/logout.svg';

function View(props) {

    const {
        nombre,
        detencionHandler,
        datosHandler,
        medicaHandler,
        logout
    } = props;

    return (
        <Fragment>
            <div className="container">
                <div className="row mt-3">
                    <div className="col-10 col-md-10 col-sm-10 col-lg-10">

                    </div>
                    <div className="col-2 col-md-2 col-sm-2 col-lg-2">
                        <a onClick={logout} href="#">
                            <img src={LogoutIcon} alt="Logout" width="30" />
                        </a>
                    </div>
                </div>
                <form>
                    <div className="row justify-content-md-center mt-3">
                        <div className="col-md-12 col-lg-6 mb-5">

                            <h3 className="text-center">Hola {nombre}, ¿cómo podemos ayudar?</h3>
                            <button onClick={detencionHandler} className="btn texto-boton boton border col-md-12 mb-5 p-5 mt-3"> <img className="icono" src={gavel}></img><br />   Me detuvieron, necesito un abogado.</button>
                            <button onClick={medicaHandler} className="btn border texto-boton col-md-12 mb-5 p-5"> <img className="icono" src={hospital} /><br /> Necesito auxilio médico.</button>
                            <p className="text-center justify-content-md-center mt-3">Para ganar tiempo en caso de un incidente, confirma tus datos <a href="#"><span onClick={datosHandler} className="texto">aquí.</span></a> </p>
                        </div>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}
export default View;

