import React from 'react';
import View from './inicio-view';
import { withRouter } from 'react-router-dom';

class Inicio extends React.Component {

    state = {
        nombre: ''
    }

    render() {
        return (
            <View
                nombre={this.state.nombre}
                detencionHandler={this.detencionHandler}
                datosHandler={this.datosHandler}
                medicaHandler={this.medicaHandler}
                logout={this.logout}
            />
        );
    }

    logout = (e) => {
        e.preventDefault();
        localStorage.clear();
        // this.props.history.push('/');
        window.location.replace('/');
    }

    async componentDidMount() {

        var id = localStorage.getItem('userId');

        if (id == null) {
            // window.location.replace('/');
            this.props.history.push('/');
        }

        var name = localStorage.getItem('nombre');
        this.setState({
            nombre: name
        });
    }

    detencionHandler = async (e) => {
        e.preventDefault();
        this.props.history.push('/detencion/1');
        // window.location.replace('/detencion/1');
    }

    medicaHandler = async (e) => {
        e.preventDefault();
    }

    datosHandler = async (e) => {
        e.preventDefault();
        this.props.history.push('/datos');
        // window.location.replace('/datos');
    }
}

export default withRouter(
    (Inicio)
)
